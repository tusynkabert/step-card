import { deleteCardFunc } from './index.js'
import { editCardFunc } from './editCardFunctions.js';

const eventsCardVisit = (btn) => {

    btn.addEventListener('click', (e) => {
        e.stopImmediatePropagation()
        e.stopPropagation()

        const el = btn

        const typeAction = el.getAttribute('data-action');

        if (typeAction) {

            const id = el.getAttribute('data-card-id');

            switch (typeAction) {

                case 'edit':
                    editCardFunc(id);
                    break;

                case 'del':
                    deleteCardFunc(id);
                    break;

            }
        }

    })
}

export default eventsCardVisit;