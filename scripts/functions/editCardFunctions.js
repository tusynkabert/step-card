import { addUniqueFieldsModal } from "./addUniqueFieldsModal.js";

function editCardFunc(id) {
  const modal = document.querySelector("#createVisitModal");
  modal.classList.add("active");

  const editCardId = document.getElementById('editCardId');
  editCardId.value = id;

  setupFormForEditing(modal);
  populateFormWithData(id);
}

function setupFormForEditing(modal) {
  const submitBtn = document.getElementById("createVisitFormBntSubmit");
  const formTitle = document.querySelector(".create-visit__title");

  modal.setAttribute("data-mode", "edit");

  if (modal.getAttribute("data-mode") === "edit") {
    formTitle.textContent = "- Edit visit -";
    submitBtn.textContent = "Save change";
  }
}

function extractVisitDataFromCard(card) {
  const doctorType = card.querySelector(".card-visit-desc").textContent.trim().toLowerCase();
  const commonData = {
    goalvisit: card.querySelector(".card-visit-title").textContent,
    description: card.querySelector(".card-description").textContent,
    status: card.querySelector(".card-status").textContent,
    urgency: card.querySelector(".card-urgency").textContent,
    clientname: card.querySelector(".card-clientName").textContent,
  };

  let specificData = {};
  switch (doctorType) {
    case "therapist":
      specificData = {
        age: card.querySelector(".card-age").textContent,
      };
      break;
    case "cardiologist":
      specificData = {
        age: card.querySelector(".card-age").textContent,
        bp: card.querySelector(".card-bp").textContent,
        weight: card.querySelector(".card-weight").textContent,
        diseases: card.querySelector(".card-diseases").textContent,
      };
      break;
    case "dentist":
      specificData = {
        lastVisit: card.querySelector(".card-lastVisit").textContent,
      };
      break;
    default:
      break;
  }

  return { ...commonData, ...specificData, doctor: doctorType };
}

function populateFormWithData(id) {
  const card = document.querySelector(`#card-visit-${id}`);
  const data = extractVisitDataFromCard(card);

  const createCardForm = document.querySelector("#createVisitForm");

  const doctorTypeMap = {
    cardiologist: "cardiologist",
    dentist: "dentist",
    therapist: "therapist",
  };

  const doctorSelect = createCardForm.querySelector("#doctorType");
  if (doctorSelect) {
    const doctorType = doctorTypeMap[data["doctor"].toLowerCase()];
    if (doctorType) {
      doctorSelect.value = doctorType;
      addUniqueFieldsModal(doctorType);
    }
  }

  const fields = createCardForm.querySelectorAll("input, textarea, select");

  fields.forEach((field) => {
    if (data.hasOwnProperty(field.name)) {
      field.value = data[field.name].trim();
    }
  });
}

export { editCardFunc };