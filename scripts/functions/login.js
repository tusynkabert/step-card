import { authorizationApi } from "../api/authorization.js";
import { AUTHORIZATION_TOKEN_KEY } from "../constants/index.js";

const loginFunc = async (callbackFn) => {

    try {
        const inputLogin = document.querySelector('#input-login');
        const inputPassword = document.querySelector('#input-password');

        const userData = {
            email: inputLogin.value,
            password: inputPassword.value,
        };

        const token = await authorizationApi.login(userData);
        localStorage.setItem(AUTHORIZATION_TOKEN_KEY, token);

        callbackFn({
            authorized: true
        });
    } catch (error) {
        console.log(error);
    }
}

export default loginFunc