import { cardsApi } from "../api/cards.js";

const updateCardFunc = async (cardId, cardData) => {
    try {
        const updatedCard = await cardsApi.updateCard(cardId, cardData);

        return updatedCard;
    } catch (error) {
        console.error(error.message);
    }
}

export default updateCardFunc;