import updateCardFunc from "./updateCard.js";
import getCardsAndRender from "./getCardsAndRender.js";
import { filterVisits } from "./index.js";

const handleEditFormSubmit = async (e) => {
    e.preventDefault();

    const createCardForm = e.target;

    const data = new FormData(createCardForm);

    const formDataObject = {
        "goalvisit": "",
        "description": "",
        "doctor": "",
        "urgency": "",
        "clientname": "",
        "weight": "",
        "age": "",
        "bp": "",
        "diseases": "",
        "lastVisit": "",
        "status": "",
    }

    data.forEach((value, key) => {
        formDataObject[key] = value;
    });

    const cardId = document.getElementById('editCardId').value;

    await updateCardFunc(cardId, formDataObject);

    const filterForm = document.querySelector('.cards-filter__form');
    filterVisits(filterForm)


    createCardForm.reset();

    const modals = document.querySelectorAll('.modal');
    modals.forEach(modal => modal.classList.remove('active'))

    const modal = document.querySelector("#createVisitModal");
    const submitBtn = document.getElementById("createVisitFormBntSubmit");
    const formTitle = document.querySelector(".create-visit__title");

    modal.setAttribute("data-mode", "create");
    formTitle.textContent = "- Create visit -";
    submitBtn.textContent = "Create";

    document.getElementById('editCardId').value = '';
}

export default handleEditFormSubmit;