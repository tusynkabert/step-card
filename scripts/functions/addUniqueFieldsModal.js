const doctorTypeSelect = document.getElementById("doctorType");
const newFieldsContainer = document.querySelector(
  ".create-visit__unique-fields"
);
let currentDoctorFields = null;

function addUniqueFieldsModal(selectedDoctor) {
  if (currentDoctorFields) {
    currentDoctorFields.remove();
  }

  const newDoctorFields = document.createElement("div");

  switch (selectedDoctor) {
    case "cardiologist":
      newDoctorFields.insertAdjacentHTML(
        "afterbegin",
        `
            <label for="clientPressure" class="create-visit__pressure-label">Blood pressure:</label>
            <input required type="text" name="bp" id="clientPressure" class="create-visit__pressure-input"><br>
            <label for="clientIndex" class="create-visit__index-label">Body mass index:</label>
            <input required type="text" name="weight" id="clientIndex" class="create-visit__index-input"><br>
            <label for="clientDiseases" class="create-visit__diseases-label">Chronic diseases:</label>
            <input required type="text" name="diseases" id="clientDiseases" class="create-visit__diseases-input" placeholder=" Chronic heart diseases"><br>
            <label for="clientAge" class="create-visit__age-label">Client age:</label>
            <input required type="text" name="age" id="clientAge" class="create-visit__age-input">
            `
      );
      break;
    case "dentist":
      newDoctorFields.insertAdjacentHTML(
        "afterbegin",
        `
            <label for="clientLastDate" class="create-visit__date-label">Last visit was:</label>
            <input required type="text" name="lastVisit" id="clientLastDate" class="create-visit__date-input" placeholder="DD.MM.YYYY">
            `
      );
      break;
    case "therapist":
      newDoctorFields.insertAdjacentHTML(
        "afterbegin",
        `
            <label for="clientAge" class="create-visit__age-label">Client age:</label>
            <input required type="text" name="age" id="clientAge" class="create-visit__age-input">
            `
      );
      break;
    default:
      break;
  }

  newFieldsContainer.append(newDoctorFields);
  currentDoctorFields = newDoctorFields;
}

export { doctorTypeSelect, newFieldsContainer, addUniqueFieldsModal };
