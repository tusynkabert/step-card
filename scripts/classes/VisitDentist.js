import { Visit } from "./Visit.js";

export class VisitDentist extends Visit {
    constructor({ doctor, lastVisit, title, clientName, description, urgency, status }) {
        super({ doctor, title, clientName, description, urgency, status })
        this.lastVisit = lastVisit
    }

    getVisitData() {
        return {
            ...this.getCommonVisitData(),
            lastVisit: this.lastVisit
        }
    }

    createVisitToDoctor() {
        this.createVisit(this.getVisitData())
    }
}