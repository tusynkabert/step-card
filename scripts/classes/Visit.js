import { createCardFunc, getCardsAndRender } from "../functions/index.js";

export class Visit {
    constructor({ doctor, title, clientName, description, urgency, status }) {
        this.doctor = doctor;
        this.title = title
        this.clientName = clientName
        this.description = description
        this.urgency = urgency
        this.status = status
    }

    async createVisit(data) {
        const { title, description, doctor, bp = '50', age = 30, weight = 80 } = data;

        const requestData = {
            title,
            description,
            doctor,
            bp,
            age,
            weight
        }

        const newCardData = await createCardFunc(requestData);

        if (newCardData) {
            document.querySelectorAll('.modal').forEach((m) => m.classList.remove('active'))
            getCardsAndRender()
        }

    }

    getCommonVisitData() {
        return {
            doctor: this.doctor,
            title: this.title,
            clientName: this.clientName,
            description: this.description,
            urgency: this.urgency,
            status: this.status
        }
    }

    edit(id) {

    }

    delete(id) {

    }

}