import { Visit } from "./Visit.js";

export class VisitTherapist extends Visit {
    constructor({ doctor, age, title, clientName, description, urgency, status }) {
        super({ doctor, title, clientName, description, urgency, status })
        this.age = age
    }

    getVisitData() {
        return {
            ...this.getCommonVisitData(),
            age: this.age
        }
    }

    createVisitToDoctor() {
        this.createVisit(this.getVisitData())
    }
}