# https://mockapi.io/login

login: doctor@gmail.com
pass: doctor

# cards input

{
title: 'Визит к кардиологу',
description: 'Новое описание визита',
doctor: 'Cardiologist',
bp: '24',
age: 23,
weight: 70
}

# cards output

`{
"id": 1,
"title": "Визит к кардиологу",
"description": "Новое описание визита",
"doctor": "Cardiologist",
"bp": "24",
"age": 23,
"weight": 70
}
`

#
